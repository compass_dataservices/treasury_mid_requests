// ==UserScript==
// @name         CC Request Robot
// @namespace    http://compass-usa.com
// @version      1.8.6
// @updateURL    https://gist.github.com/WisdomWolf/9d9df8d8f0c5f5df053f91f4f88754ab/raw/CC_Request.user.js
// @description  Automate Treasury form
// @author       WisdomWolf
// @match        https://remedyweb.compass-usa.com/arsys/forms/remedyprod/TREAS%3ARequest/Field/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';


function selectVendor() {
    paymentVendor.click();
    setTimeout(function() {
        var menuTable = $('.MenuTableContainer');
        var innerTable = menuTable.children().children().children();
        innerTable.children().eq(2).click();
    }, 1000);
}

var unitNumber = $('#arid_WIN_0_1000005060');

var paymentVendor = $('#arid_WIN_0_536870951'); //"Agilysys"
var paymentSoftware = $('#arid_WIN_0_536870952'); //"Infogenesis"
var tipSelection = $('#arid_WIN_0_536870931'); //"No"
var ccUtilized = $('#arid_WIN_0_777804034'); //"Cafe"
var avgTicketSize = $('#arid_WIN_0_777804035'); //"<$25"
var storeName = $('#arid_WIN_0_536870937'); //"Fidel-Spartan"
var connType = $('#arid_WIN_0_536870917'); //"Compass High Speed"
var compassRouter = $('#arid_WIN_0_536871107'); //"Yes"
var unitNumBilling = $('#arid_WIN_0_777804345'); //"21128"
var gatewayMiddleware = $('#arid_WIN_0_536870955'); //"FreeWay"
var numRegisters = $('#arid_WIN_0_777804050'); //"8"
var contactlessReader = $('#arid_WIN_0_777804346'); //"No"


var onSiteContact = $('#arid_WIN_0_1000005068');
var unitPhone = $('#arid_WIN_0_1000005069');
var unitEmail = $('#arid_WIN_0_1000005071');
var posVersion = $('#arid_WIN_0_536870921');
var posServicePack = $('#arid_WIN_0_536870958');
var posHotFix = $('#arid_WIN_0_536870923');
var installVendor = $('#arid_WIN_0_536870953');
var installContactName = $('#arid_WIN_0_536870959');
var installContactPhone = $('#arid_WIN_0_536870960');
var submitButton = $('#WIN_0_777804162').children();
var commentsBox = $('#arid_WIN_0_770600501');
var commentAdd = $('.btntextdiv').eq(2);


var unitData;
var reviewFields = [];

function updateField(element, value, review) {
    element.val(value);
    triggerChange(element);
    if (review) {
        markFieldForReview(element);
    }
}

function triggerChange(element) {
    if (element.length != undefined) {
        element = element[0];
    }
    if ("createEvent" in document) {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        element.dispatchEvent(evt);
    }
    else {
        element.fireEvent("onchange");
    }
}

function enterData() {
	try {
		unitData = $.getJSON('https://tpa.compassfss.com/get_json/' + unitNumber.val());
	} catch (err) {
		if (confirm("Unable to pull unit data from server.  This may be because you haven't accepted the certificate yet.  Would you like to browse to the site to do so?")) {
			var win = window.open("https://tpa.compassfss.com", '_blank');
			win.focus();
		}
	}


    updateField(tipSelection, "No");
    updateField(ccUtilized, "Cafe");
    updateField(avgTicketSize, "<$25");
    updateField(connType, "Compass High Speed");
    updateField(compassRouter, "Yes");
    updateField(contactlessReader, "No");
	markFieldForReview(numRegisters);
    //updateField(installContactName, 'Agilysys');
    //updateField(installContactPhone, 'Agilysys');

	setTimeout(fillFields, 1000);
}

function countTerms(device_array) {
  var qty = 0;
  $.each(device_array, function(index, value) {
    qty += value.qty;
  });
  return qty;
}

function fillFields() {
	var data = $.parseJSON(unitData.responseText);

    if (data.network_devices.pos_terms[0].type == 'simphony') {
        updateField(paymentVendor, "MICROS");
        updateField(paymentSoftware, "Simphony");
        updateField(posVersion, '2.7');
        updateField(posServicePack, '4');
        updateField(posHotFix, '4');
        updateField(installVendor, 'Micros');
        updateField(gatewayMiddleware, "Freeway", true);
    } else {
        updateField(paymentVendor, "Agilysys");
        updateField(paymentSoftware, "Infogenesis");
        updateField(posVersion, '3.0');
        updateField(installVendor, 'Agilysys');
        updateField(gatewayMiddleware, "FreeWay", true);
    }

	updateField(onSiteContact, data.unit_contact.name);
    updateField(unitPhone, data.unit_contact.phone);
    updateField(unitEmail, data.unit_contact.email);

	var shortName = data.unit_name.slice(0,14);
	updateField(storeName, shortName, true);

	updateField(unitNumBilling, data.unit_number, true);

    var numTerminals = countTerms(data.network_devices.pos_terms);
    updateField(numRegisters, numTerminals, true);

    var golive_date = new Date(Date.parse(data.golive_date)).toLocaleDateString();
    updateField(commentsBox, 'Golive: ' + golive_date);
    commentAdd.click();
}

function markFieldForReview(field) {
	field.css('background-color', 'yellow');
	field.bind('focusout', function() {
		field.css('background-color', '');
        reviewFields.splice($.inArray(field, reviewFields), 1);
	});
    if ($.inArray(field, reviewFields) < 0) {
        reviewFields.push(field);
    }
}

Mousetrap.bind('ctrl+s', function(e) {
    submitButton.click();
    return false;
});


Mousetrap.bind('ctrl+shift+f', function(e) {
    var data = prompt('Input JSON info:');
    enterData();
    return false;
});

Mousetrap.bind('ctrl+shift+u', enterData);
Mousetrap.bind(['ctrl+shift+x', 'alt+x'], function() {
	console.log('removing review handlers from submit button');
	alert('Submit Handlers removed');
	submitButton.unbind();
});

setTimeout(enterData, 1000);
